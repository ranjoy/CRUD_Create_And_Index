<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP136058\Book\Book;

$book=new Book();
$book->prepare($_POST);
$book->store();