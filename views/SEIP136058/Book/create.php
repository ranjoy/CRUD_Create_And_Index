<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    <script src="jquery.min.js"></script>
    <script src="bootstrap.min.js"></script>
</head>
<body bgcolor="#5f9ea0">


<div class="container">
    <h2><center>Add Book Form</center></h2>
    <form class="form-horizontal" role="form" method="post" action="store.php">
        <div class="form-group">
            <label class="control-label col-sm-2" for="email">Book Title</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="id" name="title" placeholder="Enter Book Title">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
    </form>
</div>

</body>
</html>
